'use strict';

import gulp from 'gulp';
import megaplan from 'megaplanjs';
import _ from 'lodash';
import fs from 'fs';
import translit from 'transliteration.cyr';
import marked from 'marked';
import moment from 'moment';
import 'moment-precise-range-plugin';

moment.locale('ru');

const projects = [
    1001297,
    1001298
];

var client;
var store         = require('./store.json');
var storeTasks    = require('./tasks.json');
var storeComments = require('./comments.json');
var auth          = require('./auth.json');

function authClient() {
    client = new megaplan.Client('agroindex.megaplan.ru').auth(auth.email, auth.password);
}

function saveJSON(file, data) {
    fs.writeFile(`${file}.json`, JSON.stringify(data), error => {
        if (error) return errorHandler(error);
        console.log(`The file ${file}.json was saved!`);
    });
}

const saveStore         = () => saveJSON('store', store);
const saveStoreTasks    = () => saveJSON('tasks', storeTasks);
const saveStoreComments = () => saveJSON('comments', storeComments);
const errorHandler      = error => console.log('ERROR: ', error);


function storeTasksList(query = {}, next = () => {}) {
    query.limit = 100;
    if (!query.offset) query.offset = 0;

    console.log('storeTasks... ' + query.offset);
    client.tasks(query).send(tasks => {
        console.log('storeTasks ok');
        tasks = _.values(tasks);
        if (!store.tasks) store.tasks = [];
        store.tasks = [...store.tasks, ...tasks];
        
        query.offset += 100;

        if (!tasks.length || tasks.length < 100) {
            console.log('storeTasks: END');
            next();
        } else {
            storeTasksList(query, next);
        }
    }, errorHandler);
}



function storeProjecs(next = () => {}, index = 0) {
    var projectId = projects[index];
    if (projectId) {
        storeTasksList({project_id:projectId, only_actual: true}, () => {
            storeProjecs(next, index + 1);
        });
    } else {
        console.log('storeProjecs: END');
        next();
    }
}

gulp.task('pull-tasks', cb => {
    store = {};
    authClient();
    client.on('auth', function (res, err) {
        var data = {};

        client.projects().send(data => {
            store.projects = _.values(data.projects);

            storeProjecs(function() {
                saveStore()
                cb();
            });
        }, errorHandler)
    });
});

function storeTaskContent(next = () => {}, index = 0)  {
    var task = store.tasks[index];
    
    if (!task) {
        return next();
    }

    var id = task.id;
    if (storeTasks[id]) {
        console.log('Skip => ', index);
        storeTaskContent(next, index + 1);
    } else {
        var _storeComments = (id) => {
            client.task_comments(id).send(({comments}) => {
                storeComments[id] = _.values(comments);
                console.log('Task => ', index);
                saveStoreTasks();
                saveStoreComments();
                storeTaskContent(next, index + 1);
            });
        }
        
        client.task(task.id).send(({task}) => {
            storeTasks[id] = task;
            _storeComments(task.id);
        }, errorHandler);
    }
}

function pullContent(force = false) {
    if (store.tasks.length === _.keys(storeTasks).length) {
        storeTasks    = {};
        storeComments = {};
    }
    authClient();
    client.on('auth', function (res, err) {
        storeTaskContent(() => {
            saveStoreTasks();
            saveStoreComments();
        });
    });
}

gulp.task('pull-content', cb => {
    pullContent();
});

gulp.task('pull-content-force', cb => {
    storeTasks    = {};
    storeComments = {};
    pullContent();
});

function makeHtml(title, content) {
    return `<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>${title}</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        blockquote{font-size:12px; padding:0 10px; margin: 10px 0}
        img.avatar{display:inline-block; border-radius:50%;}
        .panel-heading img.avatar {margin-top: -5px}
        .responsible {display:inline-block; margin-left:30px}
    </style>
  </head>

  <body>
    <div class="container">${content}</div>
  </body></html>`;
}
function renderAttaches(attaches) {
    var html = '';
    if (attaches) {
        _.values(attaches).forEach(attach => {
            var url = 'https://agroindex.megaplan.ru/' + attach.url.replace(/^[\/]+api(.*)$/, '$1');
            if (attach.url.indexOf('.jpg') > 0 || attach.url.indexOf('.png') > 0) {
                var thumb = 'https://pc11.megaplan.ru/hosts/agroindex.megaplan.ru/200x200/' + attach.url.replace(/^[\/]+api(.*)\/[^\/]+$/, '$1');
                html += `<a href="${url}" target="_blank" style="float:left;margin-right:10px" class="thumbnail"><img src="${thumb}" /><div class="caption">${attach.name}</div></a>`;
            } else {
                html += `<a href="${url}" target="_blank">${attach.name}</a><br>`;
            }
        });
    }
    return html;
}
function renderComments(id) {
    var html = '';
    if (storeComments[id] && storeComments[id].length) {
        html += '<hr>'
        storeComments[id].forEach(comment => {
            var text = marked(comment.text.replace('\n', '\n\n')) + renderAttaches(comment.attaches);
            var mediaClass = comment.is_unread ? ' unread-comment bg-warning panel-body' : '';
            html += `<div class="media${mediaClass}">
                <div class="media-left">
                    <a><img src="https:${comment.avatar}" width="40" /></a>
                </div>
                <div class="media-body">
                    <b class="label label-default">${comment.author.name}</b><br>
                    ${text}
                </div>
            </div>`;
        })
    }
    return html;
}
function renderPanel(content, title, task = {}) {
    var auditors = '';
    // if (task.auditors) {
    //     _.values(task.auditors).forEach(auditor => {
    //         auditors += `<span class="label label-default">${auditor.name}</span> `;
    //     })
    //     auditors = auditors ? 'Аудиторы: ' + auditors : '';
    // }
    var responsible = task.responsible ? `<span class="responsible"><small>${task.responsible.name}</small> <img class="avatar" src="http:${task.responsible.avatar}" width=30 /></span>` : '';
    title = title ? `<div class="panel-heading"><a href="https://agroindex.megaplan.ru/task/${task.id}/card/">${title}</a> <span style="float:right">${auditors}${responsible}</span></div>` : '';
    return `<div class="panel panel-default">${title}<div class="panel-body">${content}</div></div>`;
}
gulp.task('render-tasks', cb => {
    var tasks = {};
    var projectData = {};
    var childrens = {};
    store.projects.forEach(project => {
        projectData[project.id] = project;
    });
    store.tasks.forEach(task => {
        var id = task.id;
        tasks[id] = task;
        var pid = task.super_task ? task.super_task.id : 0;
        if (!childrens[pid]) {
            childrens[pid] = [];
        }
        childrens[pid].push(task);
    })
    
    var htmlAll = '';
    var group = '';
    childrens[0].forEach((task, index) => {
        var id = task.id;
        var extUrl = `https://agroindex.megaplan.ru/task/${task.id}/card/`;

        var html = '';
        html += `<br><br><a class="btn btn-primary" href="./index.html">HOME</a><hr>`;
        html += `<h1>${task.name} <a href="${extUrl}" class="btn btn-default">megaplan</a></h1>`;
        html += `<h3>${task.project.name}</h3>`;
        // console.log( storeTasks[id] );
        if (storeTasks[id]) {
            html += storeTasks[id].statement;
        }
        html += renderComments(id);

        if (childrens[id]) {
            html += `<hr>`;

            childrens[id].forEach(sub => {
                var body = storeTasks[sub.id] ? storeTasks[sub.id].statement : '';
                html += renderPanel(body + renderComments(sub.id), sub.name, storeTasks[sub.id]);
            });
        }

        var fName = translit.transliterate(task.project.name).replace(/[^\w]+/gi, '_') + '-' + translit.transliterate(task.name).replace(/[^\w]+/gi, '_');
        fName = `${fName}.html`;

        fs.writeFile('data/' + fName, makeHtml(`${task.name} - ${task.project.name}`, html));
        
        var unreadItems = 0;
        var re = /unread-comment/g;
        while (re.exec(html) !== null) {
            unreadItems++;
        }

        var highlight = '';
        if (auth.highlightTask) {
            auth.highlightTask.forEach(label => {
                if (html.search(label) >= 0) {
                    highlight += `<span class="label label-info">${label}</span> `;
                }
            });
        }
        var muted = '';
        if (auth.mutedTask) {
            auth.mutedTask.forEach(label => {
                if (html.search(label) >= 0) {
                    muted += `<span class="label label-default">${label}</span> `;
                }
            });
        }
        
        var style = task.name.indexOf('ктивный') > 0 || muted ? 'style="background:#DDD;opacity:.5"' : '';

        var makeDateLabel = (date) => {
            date      = moment(date);
            var now   = moment();
            var dateH = now.from(date, true);
            var diff  = moment.preciseDiff(now, date, true); 
            var labelClass = diff.months ? '' : (diff.days <= 1 ? 'danger' : (diff.days <= 3 ? 'warning' : ''));
            if (labelClass) {
                labelClass = ` class="label label-${labelClass}"`;
            }
            return `<small${labelClass}>${dateH}</small>`;
        }
        var isNew    = unreadItems ? '<span class="label label-warning">New</span>' : '';
        var unread   = unreadItems ? `<span class="label label-warning">${unreadItems}</span>` : '';
        var created  = makeDateLabel(task.time_created);
        var activity = makeDateLabel(task.activity);
        // console.log(task);
        // console.log( moment().from(moment(task.time_created), true) );
        
        if (group != task.project.name) {
            group = task.project.name;
            htmlAll += `<tr>
                <th colspan=10><h2>${group}</h2></th>
            </tr>`;
        }
        htmlAll += `<tr ${style}>
            <td>${index+1}</td>
            <td>${created}</td>
            <td>${activity}</td>
            <td>${unread}</td>
            <td>${highlight}${muted}</td>
            <td><a ${style} href="./${fName}">${task.name}</a></td>
            <td><a ${style} href="${extUrl}" target="_blank">ext</a></td>
        </tr>`;
    });
    
    fs.writeFile('data/index.html', makeHtml('index', `<table class="table table-striped">${htmlAll}<table>`));
});

gulp.task('default', (cb) => {
    // authClient();
    // client.on('auth', (res, err) => {
    //     client.tasks({project_id:1001298, only_actual: true}).send(data => {
    //         console.log('data => ', data);
    //     });
    // });
});